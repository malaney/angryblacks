<?php
    $pages = [
        '/index.php' => 'Home',
        '/form.php' => 'T-Shirts',
        '/contest.php' => 'Cash Prizes!'
    ];
?>
<!-- navbar -->
<nav class="navbar navbar-expand-lg navbar-light my-bg-light">
  <a class="navbar-brand" href="#">
    <img src="/images/cramberry_logo.png" width="100" alt="Cramberry Games" title="Cramberry Games" />
  </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav">
    <?php
        foreach ($pages as $url => $title) {
            $active = ($url == $_SERVER['REQUEST_URI']);
            if ($active) {
    ?>

      <li class="nav-item">
        <a class="nav-link active" href="<?php print $url; ?>"><?php print $title; ?><span class="sr-only">(current)</span></a>
      </li>
    <?php
            } else {
    ?>
      <li class="nav-item">
        <a class="nav-link" href="<?php print $url; ?>"><?php print $title; ?></a>
      </li>
    <?php
            }
        }
    ?>
    </ul>
  </div>
</nav>

<!-- navbar -->
